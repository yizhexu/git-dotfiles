####### Notes ######

# 1) Please update the user info below as needed
# 2) This .gitconfig relies on [git-subrepo](https://github.com/ingydotnet/git-subrepo) for all `subrepo` aliases
# 3) The `smd` and `smcl` submodule aliases have not been tested

####### USER ######

[user]
  name = Thomas Buico
  email = git@tombuico.com
  username = tbuico

####### MAIN CONFIG ######

[color]
  ui = auto
[color "branch"]
  current = yellow bold
  local = green bold
  remote = cyan bold
[color "diff"]
  frag = magenta bold
  meta = yellow bold
  new = green bold
  old = red bold
  whitespace = red reverse
[color "status"]
  added = green bold
  changed = yellow bold
  untracked = red bold
[core]
  autocrlf = false
  compression = 9
  editor = nano
  excludesfile = "${HOME}"/.gitignore_global
  filemode = false
  fsyncobjectfiles = true
  preloadindex = true
  trustctime = false
  whitespace = fix,-indent-with-non-tab,trailing-space,cr-at-eol
[credential]
  helper = cache --timeout=3600
  # helper = wincred
[diff]
  submodule = log
[filter "lfs"]
  clean = git-lfs clean -- %f
  smudge = git-lfs smudge -- %f
  process = git-lfs filter-process
  required = true
[http]
  postBuffer = 256m
  sslVerify = false
[lfs]
  activitytimeout = 0
[merge]
  tool = vimdiff
[mergetool]
  keepbackups = false
  keeptemporaries = false
  prompt = false
  trustexitcode = false
[pack]
  deltaCacheSize = 1
  packSizeLimit = 512m
  threads = 0
  windowMemory = 512m
[pager]
  branch = false
[pull]
  rebase = false
[push]
  default = matching
[status]
  submodulesummary = 1
[web]
  browser = google-chrome

####### ALIASES ######

[alias]
  # [add]
    a = add -v                                      # add <file> w/ verbose [NOTE: `git a .` & `git add -A` is not compatible with git 2.0]
    ad = add -vA .                                  # add all for the current directory w/ verbose
    at = add -vA :/                                 # add all for the whole tree w/ verbose
    rao = remote add origin                         # add origin to remote

  # [branch]
    b = branch - vv                                 # `git branch` with verbosity
    ba = branch -a                                  # list both remote-tracking branches and local branches
    bd = branch -d                                  # delete branch with `git branch -d <branch_name>`
    bfd = branch -D                                 # delete branch irrespective of its merged status w/ `git branch -d <branch_name>`
    bfdml = "!for i in \"$(git branch --list --merged | grep -iv \"master\\|head\")\"; do git branch -D $(echo ${i}); done"
    # ▲                                             # delete already merged local branches on local
    bfdmr = "!for i in \"$(git branch -r --list --merged | grep -iv \"master\\|head\")\"; do git push origin --delete $(echo ${i}); done"
    # ▲                                             # delete already merged remote branches on origin
    bfdlo = "!git branch --list | tr -d '*' | tr -d '[:blank:]' | grep -v \"$(git branch -r | tr -s ' ' | cut -d '/' -f2-)\" | xargs git branch -D"
    # ▲                                             # branch force delete local only branches that are not on the origin (# TODO: fix cryptic error when no branches are local only)
    bl = branch --list                              # list only local branches
    t1 = "!t1() { git branch -f $@ origin/$@ ; }; t1"
    # ▲                                             # track one specific remote branch [eg: git t1 <branch>]
    ta = "!for i in `git branch -a | grep remote | grep -v HEAD | grep -v master`; do git branch -f $(echo ${i} | cut -d/ -f3) origin/$(echo ${i} | cut -d/ -f3); done"
    # ▲                                             # track all remote branches
    update = "! u() { \
      set -e; \
      current=\"$(command git symbolic-ref --short -q HEAD)\"; \
      for branch in $(command git for-each-ref --format='%(refname)' 'refs/heads/'); do \
        branch=\"${branch#refs/heads/}\"; \
        printf \"Updating %s ... \" \"${branch}\"; \
        git checkout -q \"${branch}\"; \
        git pull -q; \
        printf 'done\n'; \
      done; \
      test -z \"${current}\" || git checkout -q \"${current}\"; \
    }; u"
    # ▲                                             # `git pull` all branches from orign [untested]

  # [checkout]
    co = checkout                                   # `git checkout`
    cob = checkout -b                               # create branch and checkout
    col = "!git checkout $(git for-each-ref --count=30 --sort=-committerdate refs/heads/ --format='%(refname:short)' | head -1)"
    # ▲                                             # checkout most recently committed and tracked branch [may require `git track` first]
    com = checkout master                           # checkout master branch

  # [clone]
    cl = clone                                      # `git clone`
    cl1 = clone --depth=1                           # shallow clone
    clb = "!clb() { git clone -b \"$1\" --single-branch \"$2\"; }; clb"
    # ▲                                             # Clone a single branch [eg: git clb ee_vispr https://gitlab-test.enwd.co.sa.charterlab.com/tmc_grp/framework.git]

  # [commit]
    ci = commit                                     # `git commit`
    ca = commit -a                                  # commit all
    ccd = "!git commit -m $(date '+%F_%H-%M')"      # commit w/ current date and time for message
    ceap = "!ceap() { git commit --amend -m \"$*\" && git push origin master --force; }; ceap"
    # ▲                                             # edit most recent commit message [after push]
    cebp = commit --amend -m                        # edit most recent commit message [before push]
    cm = commit -m                                  # commit w/ message

  # [lfs]
    # lfs = "!git-lfs"

  # [log]
    hist = log --pretty=format:'%h %ad | %s%d [%an]<%an>%Creset' --graph --date=short
    # ▲                                             # untested log formatting
    last = log -1 --stat --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative --all
    # ▲                                             # show last commit & generate a diffstat in log
    lf = log -u --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative --all
    # ▲                                             # show the history of a specific file, with diffs
    lg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative --all
    # ▲                                             # beautiful git log
    ll = log --decorate --numstat --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative --all
    # ▲                                             # list commits w/ changed files
    sum = log --pretty=format:'%h %G? %aN  %s<%an>%Creset'
    # ▲                                             # simple commit list w/o color
    dh = diff HEAD                                  # show deltas since the last commit, whether or not they have been staged for commit or not

  # [pull]
    pl = pull -v --progress                         # pull w/ verbose & progress
    plo = pull origin -v --progress                 # pull origin w/ verbose & progress

  # [push]
    ps = push -v --progress                         # push w/ verbose & progress
    pso = push origin -v --progress                 # push origin w/ verbose & progress
    psom = push -u origin master -v --progress      # push origin to master remote w/ verbose & progress

  # [submodule]
    sma = submodule add                             # add git repo as a submodule (eg: git submodule add git@github.com:url_to/submodule.git path/to/submodule)
    smab = "!smab() { git submodule add -b \"$1\" \"$2\"; }; smab"
    # ▲                                             # add git repo using a speciffic branch as a submodule (eg: git submodule add -b master git@github.com:url_to/submodule.git path/to/submodule)
    smai = "!smai() { \
      git submodule add \"$1\" \"$2\" && \
      git submodule init \"$2\" ; \
    }; smai"
    # ▲                                             # add git submodule and then init it (eg: git smai git@github.com:url_to/submodule.git path/to/submodule)
    smcl = clone --recurse-submodules               # clone and automatically initialize and update each submodule in the repository
    smd = diff --cached --submodule                 # view the diffs staged for the next commit relative to <commit> or HEAD if no commit given of a submodule
    smi = submodule init                            # initialize local .gitmodules configuration file after a clone of a repo containing submodule(s)
    smlg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative --all -p --submodule
    # ▲                                             # beautiful git log with submodule(s)
    smrm = "!smrm() { \
      git config -f .gitmodules --remove-section submodule.\"$1\" && \
      git config -f .git/config --remove-section submodule.\"$1\" && \
      git add -vA :/ && \
      git commit -m \"Removing submodule\" && \
      git rm -rf \"$1\" && \
      git rm -rf --cached \"$1\" && \
      rm -rf .git/modules/\"$1\" ; \
    }; smrm"
    # ▲                                             # remove a submodule [eg: git smrm "${relative_path_to_submodule}" "${submodule_name_for_commit_message}"]
    smsb = "!smsb() { git config -f .gitmodules submodule.\"$1\".branch \"$2\"; }; smsb"
    # ▲                                             # set a submodule to track a specific branch setting it in `.gitmodules` [eg: git ssmb <path/submodule> <branch>]
    smu = submodule update                          # fetch all data from the submodule(s) and check out the appropriate commit in the newly cloned and inited superproject
    smui = submodule update --init                  # this will update the submodules, and if they're not initiated yet, will initiate them
    smuir = submodule update --init --recursive     # update/initiate the submodules recursively
    smur = submodule update --remote                # fetch and update each submodule in the repository [basicly a git pull for each submodule(s)]
    smurr = submodule update --remote --recursive   # update all added submodules recursively
    smurs = "!smurs() { git submodule update --remote \"$1\" --depth=1; }; smurs"
    # ▲                                             # shallow submodule update of remote

  # [subrepo]
    sr = subrepo                                    # `git subrepo`
    srb = subrepo branch -dv                        # create a branch with local subrepo commits
    src = subrepo commit -dv                        # add subrepo branch to current history as a single commit
    srcl = subrepo clone -dv                        # add a repository as a subrepo in a subdir of your repository
    sri = subrepo init -dv                          # turn an existing subdirectory into a subrepo
    srpl = subrepo pull -dv                         # update the subrepo subdir with the latest upstream changes
    srpla = subrepo pull -dv --all                  # update all subrepos in the repo with the latest upstream changes
    srps = subrepo push -dv                         # push a properly merged subrepo branch back upstream
    srst = subrepo status                           # get the status of a subrepo
    srclpb = "!srclpb() { git subrepo clone -dv \"$1\" \"$2\" -b \"$3\" -f; }; srclpb"
    # ▲ - force clone specific branch [eg: git subrepo clone -dv git@gitlab.spectrumxg.com:APCoE_Performance/rispr-rest-api.git dev/aws/rispr-rest-api -b master -f]
    srup = "!srup() { git -C ${HOME}/.git-subrepo/ pull; }; srup"
    # ▲                                             # update subrepo git addon

  # [misc.]
    acp = "!git at && git ccd && git ps"            # quickly add all changes in tree, commit with current date and time, and push with verbosity and progress
    clnmaster = "!git checkout -q master -f && git fetch origin && git reset --hard origin/master && git clean -fdx"
    # ▲                                             # returns master branch to state from most recent commit on the origin server
    cp = cherry-pick                                # `git cherry-pick`
    dc = diff --cached                              # view the diffs staged for the next commit relative to <commit> or HEAD if no commit given
    dl = "!dl() { folder=`sed 's|tree/master|trunk|' <<<\"$@\"` && folder=`sed 's|blob/master|trunk|' <<<\"$folder\"` && svn export \"$folder\" ; }; dl"
    # ▲                                             # download a single directory or file inside of a large git repo
    dump = cat-file -p                              # [untested]
    ignore = "!gi() { curl -L -s -k https://www.gitignore.io/api/$@ ; }; gi"
    # ▲                                             # api call to pre-populated an ignore file for the project's OS, IDE, or Language [eg: git ignore java,go > .gitignore]
    la = "!grep -A5000 -m1 -e '\\[alias\\]' ${HOME}/.gitconfig"
    # ▲                                             # list all aliases
    rpo = remote prune origin                       # Deletes all stale remote-tracking branches
    st = status -sb                                 # status in short-format & show the branch and tracking info
    type = cat-file -t                              # [untested]
    unstage = reset HEAD --                         # interactively select hunks different between the index and HEAD
    wtp = worktree prune                            # prune working tree information in $GIT_DIR/worktrees
